import re
import random
from telegram.ext import BaseFilter


class Base_action:
    def __init__(self, keyword):
        """Use Base Action to create a action handler.

        keyword: action keyword.
        """

        # deprecated
        self._ignore_list = ['http',
                            'ftp',
                            'socks']

        # keyword
        self.keyword = keyword

        # who you want to do with
        self.object = '0replaceargs0'

    def set_white_list(self, white_list):
        """set white list"""
        self.white_list = white_list

    def add_ignore_list(self, unit):
        """add unit to ignore list"""
        self._ignore_list.append(unit)

    def set_reject(self, reject):
        """set reject message"""
        self.reject = reject

    def set_action(self, action_list):
        """set action what you want, for example:
           'pia' + self.object
           self.object + '不乖'
        """
        self.actions = action_list

    def run(self, text):
        """start job"""
        self.text = text
        self.object = re.sub(''.join((self.keyword, '\s+')), '', self.text.strip(' ,.;，。；'))
        self.object = re.sub(self.keyword, '', self.object)

        for i in range(len(self.actions)):
            self.actions[i] = self.actions[i].replace('0replaceargs0', self.object, 1)

        if not self.text.startswith(self.keyword):
            return 'ignore'

        elif self.object.lower() in self.white_list:
            return self.reject

        else:
            return random.choice(self.actions)

    def custom_run(self, fn):
        """custom run"""
        self.custom_run_fn = fn
        return self.custom_run_fn()


class Custom_Filter(BaseFilter):
    def __init__(self, keyword):
        self._keyword = keyword

    def filter(self, message):
        return self._keyword in message.text
