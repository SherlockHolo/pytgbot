import requests

from arch_search import arch_search
from bs4 import BeautifulSoup


class Search:
    def search(self, raw_query):
        query, engine = raw_query.split(' by ')

        if engine == 'duck':
            return self.duck_duck_go(query)

        elif engine == 'arch':
            return self.arch_linux(query)

        else:
            return None

    def duck_duck_go(self, query):
        url = "https://duckduckgo.com/html/?"
        data = {"q": query}

        s = requests.session()

        try:
            r = s.get(url, params=data,
                      headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0'})
        except IOError:
            return

        soup = BeautifulSoup(r.text, 'lxml')

        for link in soup.find_all(name='a', attrs={'class': 'result__snippet'}):
            result = link.get('href')
            # print(result)
            if result is not None:
                if result.startswith('https://duckduckgo.com/y.js?u3'): continue
                # if result.startswith('/l/?kh=-1&uddg='):
                #     result = result.replace('/l/?kh=-1&uddg=', '')
                #     result = result.replace('%3A', ':')
                #     result = result.replace('%2F', '/')
                #     return result
                return result

        return None

    def arch_linux(self, query):
        return arch_search(query)


if __name__ == '__main__':
    search = Search()
    q = 'google by duck'
    # q = 'python by arch'
    r = search.search(q)
    print(r)
