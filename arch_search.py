import requests
from debug_tools import run_time


official_result = '''
Name: {0}
Version: {1}
Description: {2}
Repo: {3}
url: https://www.archlinux.org/packages/{3}/{4}/{0}
'''

AUR_result = '''
Name: {0}
Version: {1}
Desc: {2}
Repo: AUR
url: https://aur.archlinux.org/packages/{0}
'''


@run_time
def arch_search(raw_query):
    if raw_query == '':
        return '/yaourt@SherlockHolo_bot package [repo]'
    _query = raw_query.split(' ')

    stable_repos = ('Core', 'Extra', 'Community', 'Multilib')
    testing_repos = ('Testing', 'Community-Testing', 'Multilib-Testing')
    archs = ('any', 'x86_64')

    # setting up the official repo to query
    official_payload = {
            'name': _query[0],
            'arch': archs
            }
    if len(_query) >= 2:
        # Now the '[repo]' command line argument supports specifying the exact
        # repo to query, e.g. Core, Community or Multilib-Testing. Hope one day
        # archweb will let us query in repos like [gnome-unstable] or
        # [kde-unstable].
        # 'testing' is a reserved word to query in all official testing repos.
        official_repo_only = True
        if _query[1] == 'testing':
            official_payload['repo'] = testing_repos
        else:
            official_payload['repo'] = _query[1]  # potential code injection?
    else:
        official_repo_only = False
        official_payload['repo'] = stable_repos

    # aur repo
    aur_payload = {
            'v': '5',
            'type': 'search',
            'arg': _query[0]
            }

    official_url = 'https://www.archlinux.org/packages/search/json'
    aur_url = 'https://aur.archlinux.org/rpc'

    # firstly check official repos
    yaourt = requests.get(official_url, params=official_payload)
    yaourt_json = yaourt.json()

    # note: archweb won't tell us why the request is invalid, but the most
    # possible reason here is non-exist repo name.
    if not yaourt_json['valid']:
        return "invalid request, maybe repo doesn't exist"

    if yaourt_json['results'] != []:
        json_result = yaourt_json['results'][0]
        Name = json_result['pkgname']
        Version = json_result['pkgver']
        Desc = json_result['pkgdesc']
        Repo = json_result['repo']
        Arch = json_result['arch']
        return official_result.format(Name, Version, Desc, Repo, Arch)

    # if package can't be found in official repos, and user didn't specify a
    # repo to query, go to AUR:
    if not official_repo_only:
        AUR_yaourt = requests.get(aur_url, params=aur_payload)
        AUR_yaourt_json = AUR_yaourt.json()

        if AUR_yaourt_json['resultcount'] != 0:
            json_result = AUR_yaourt_json['results'][0]
            Name = json_result['Name']
            Version = json_result['Version']
            Desc = json_result['Description']
            return AUR_result.format(Name, Version, Desc)

    # finally
    return None


if __name__ == '__main__':
    import sys
    print(arch_search(' '.join(sys.argv[1:])))
