#!/usr/bin/env python3

from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
import logging
import json
import sys

# bot function
from arch_search import arch_search
from google_search import google_search
from unity_search import Search
from yd_translate import yd_translate
from eat import eat as _eat, eat_filter
from pia import pia as _pia, pia_filter

# for log
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

# token
with open(sys.argv[1], 'r') as f:
    bot_token = json.load(f)

bot = Updater(token=bot_token['token'])
dispatcher = bot.dispatcher


# bot action
def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text='Hi~ 咱是夏洛克狼的bot~')


def whatcanyoudo(bot, update):
    cando = '''/google 搜索东西
/yaourt 搜索Arch Linux的包
/translate 翻译单词
以上都是拥有一丝夏洛克狼灵魂的bot和小蟒蛇一起工作的结果😋'''

    bot.send_message(chat_id=update.message.chat_id, text=cando)


# google search
def google(bot, update, args):
    query = ' '.join(args)
    result = google_search(query)
    bot.send_message(chat_id=update.message.chat_id, text=result)


# search arch package
def yaourt(bot, update, args):
    query = ' '.join(args)
    result = arch_search(query)
    bot.send_message(chat_id=update.message.chat_id, text=result)


unity_search_engine = Search()


def unity_search(bot, update, args):
    query = ' '.join(args)
    result = unity_search_engine.search(query)
    bot.send_message(chat_id=update.message.chat_id, text=result)


# eat somebody
def eat(bot, update):
    text = _eat(update.message.text)
    if text == 'ignore':
        return None

    else:
        bot.send_message(chat_id=update.message.chat_id, text=text)


eat_filter = eat_filter()


# pia somebody
def pia(bot, Update):
    bot.send_message(chat_id=Update.message.chat_id, text=_pia(Update.message.text))


pia_filter = pia_filter()


# translate
def translate(bot, update, args):
    query = ' '.join(args)
    result = yd_translate(query)
    bot.send_message(chat_id=update.message.chat_id, text=result)


# run bot
start_handler = CommandHandler('start', start)
google_handler = CommandHandler('google', google, pass_args=True)
yaourt_handler = CommandHandler('yaourt', yaourt, pass_args=True)
unity_search_handler = CommandHandler('search', unity_search, pass_args=True)
translate_handler = CommandHandler('translate', translate, pass_args=True)
whatcanyoudo_handler = CommandHandler('whatcanyoudo', whatcanyoudo)
eat_handler = MessageHandler(eat_filter, eat)
pia_handler = MessageHandler(pia_filter, pia)

dispatcher.add_handler(start_handler)
dispatcher.add_handler(google_handler)
dispatcher.add_handler(yaourt_handler)
dispatcher.add_handler(unity_search_handler)
dispatcher.add_handler(translate_handler)
dispatcher.add_handler(whatcanyoudo_handler)
dispatcher.add_handler(eat_handler)
dispatcher.add_handler(pia_handler)

bot.start_polling()
