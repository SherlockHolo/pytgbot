import re
import random
from telegram.ext import BaseFilter

white_list = ['wolf',
              'horo',
              'holo',
              'sherlock holo',
              '我',
              'kenookamihoro',
              'sherlock-holo',
              'sherlockholo',
              'ホロ']

ignore_list = ['http',
               'ftp',
               'socks']

def eat(text):
    for ignore in ignore_list:
        if ignore in text:
            return 'ignore'

    if text.index('eat') != 0:
        return 'ignore'

    eat_object = re.sub('eat\s+', '', text.strip(' ,.;，。；'))
    eat_object = re.sub('eat', '', eat_object)
    if eat_object.lower() in white_list:
        return '不给吃'
    elif '狼' in eat_object:
        return '不给吃'

    eat1 = '吃掉' + eat_object + '😋'
    eat2 = eat_object + '好不好吃呢?😋'
    eat3 = '在' + eat_object + '头上洒上孜然粉，一口咬下去😋'
    return random.choice((eat1, eat2, eat3))


class eat_filter(BaseFilter):
    def filter(self, message):
        return 'eat' in message.text
