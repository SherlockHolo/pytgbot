import requests
import json

with open('config.json', 'r') as f:
    config = json.load(f)

key = config['google_API_key']
cx = config['cx']


def google_search(query):
    if query != '':
        # query = query.lower()
        url = "https://www.googleapis.com/customsearch/v1element?cx={}&key={}&q={}&start=1".format(cx, key, query)
        google = requests.get(url)
        r = google.json()['results'][0]['clicktrackUrl']
        result = requests.get(r)
        return result.url
    else:
        return "/google@SherlockHolo_bot [query]"
